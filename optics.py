# std
import Queue
import sched, time

# my
from classes import *
from clustering import make_dots

# misc
from mongoengine import connect


def OPTICS(ep, minpts):
    ordered_dot_list = []
    dots = Dot.objects
    if dots:
        for current_dot in dots:
              if current_dot.is_used:
                  continue
              neibours = current_dot.getNeighborsAndMinD(ep, dots)
              if not current_dot in ordered_dot_list:
                  ordered_dot_list.append(current_dot)
              coredist = set_core_distance(current_dot, ep, minpts, dots)
              current_dot.is_used = True
              current_dot.save()
              if (coredist != -1):
                  seeds = Queue.PriorityQueue()
                  seeds = update(neibours, seeds, coredist)
                  while not seeds.empty():
                     current_dot = seeds.get()
                     neibours1 = current_dot.getNeighborsAndMinD(ep, dots)
                     current_dot.is_used = True
                     current_dot.save()
                     if current_dot in ordered_dot_list:
                         ordered_dot_list.remove(current_dot)
                     ordered_dot_list.append(current_dot)
                     coredist = set_core_distance(current_dot, ep, minpts, dots)
                     if (coredist != -1):
                        seeds = update(neibours1, seeds, coredist)

    return ordered_dot_list



def OPTICS_live(ep, minpts, ordered_dot_list):
    s = sched.scheduler(time.time, time.sleep)
    index = "000000000000000000000000"
    while 1:
        dots = Dot.objects(id__gt=index)
        if dots:
            print "YES"
            index=dots[len(dots)-1].id
            for current_dot in dots:
                  if current_dot.is_used:
                      continue
                  neibours = current_dot.getNeighborsAndMinD(ep, dots)
                  if not current_dot in ordered_dot_list:
                      ordered_dot_list.append(current_dot)
                  coredist = set_core_distance(current_dot, ep, minpts, dots)
                  current_dot.is_used = True
                  current_dot.save()
                  if (coredist != -1):
                      seeds = Queue.PriorityQueue()
                      seeds = update(neibours, seeds, coredist)
                      while not seeds.empty():
                         current_dot = seeds.get()
                         neibours1 = current_dot.getNeighborsAndMinD(ep, dots)
                         current_dot.is_used = True
                         current_dot.save()
                         if current_dot in ordered_dot_list:
                             ordered_dot_list.remove(current_dot)
                         ordered_dot_list.append(current_dot)
                         coredist = set_core_distance(current_dot, ep, minpts, dots)
                         if (coredist != -1):
                            seeds = update(neibours1, seeds, coredist)
        else:
            time.sleep(2)
        print ordered_dot_list
        return ordered_dot_list






def set_core_distance(dot, ep, minpts, dots):
    neibours = dot.getNeighborsAndMinD(ep, dots)
    if len(neibours) >= minpts:
        distances = neibours.values()
        if 0 in distances:
            distances.remove(0)
        if distances:
            dot.coredistance = min(list(distances))
            dot.save()
            return dot.coredistance
    
    return -1


def update(neibours, seeds, coredist):
    for other_dot, distance in neibours.iteritems():
        if not other_dot.is_used:
            new_reach_dist = max(coredist, distance)
            if other_dot.reachability_distance == -1 or \
                    new_reach_dist < other_dot.reachability_distance:
                other_dot.reachability_distance = new_reach_dist
                other_dot.save()
                seeds.put(other_dot, new_reach_dist)
    
    return seeds


def make_clusters_optics(ordered_list):
    cluster = None
    clusters = []
    all_pkt_count = all_output_num = dot_count = {}
    for dot in ordered_list:
        if dot.reachability_distance == -1:
            cluster = Cluster(alg_type=1)
            cluster.save()
            clusters.append(cluster)

        dot.cluster = cluster
        dot.save()

        if cluster in dot_count:
            all_pkt_count[cluster] += dot.packet_count
            all_output_num[cluster] += dot.output_num
            dot_count[cluster] += 1
        else:
            all_pkt_count[cluster] = dot.packet_count
            all_output_num[cluster] = dot.output_num
            dot_count[cluster] = 1
    
    return clusters
