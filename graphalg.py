# std
from Queue import Queue

# my
from classes import *


def graphalg(r):
	dots = Dot.objects(cluster__exists=False)
	while dots.count():
		cluster = Cluster()
		cluster.save()
		print "New cluster generated " + str(cluster.id)
		
		queue = Queue()
		queue.put(dots.first())
		while not queue.empty():
			dot = queue.get()
			if dot.cluster is None:
				dot.cluster = cluster
				print "Mark Dot(%s) as %s" % (str(dot.id)[-2:], str(dot.cluster.id)[-2:])
				dot.save()
				neighbors = dot.get_neighbors(r, Dot.objects(cluster__exists=False))
				for neighbor in neighbors:
					if neighbor not in queue.queue:
						queue.put(neighbor)

		dots = Dot.objects(cluster__exists=False)

	return Cluster.objects
